﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;

    public GameObject projectilePrefab;
    public Transform shotSpawn;
    public float shotSpeed = 7f;

    Quaternion m_Rotation = Quaternion.identity;
    // Start is called before the first frame update

    public GameObject noteHolder;//drag and drop reference to parent: can be toggled to hide
    public Text noteText; //drag and drop reference to text box of note in the canvas
    public bool noteOpened = false;

    Transform currCheckpoint;

    void Start()
    {
        //Set variable references
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //create both horizontal and vertical variables
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        //set the values for the vector, then normalize the values
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        //determine if there is player input
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);

        //combine vertical and horizontal into single bool
        bool isWalking = hasHorizontalInput || hasVerticalInput;

        m_Animator.SetBool("isWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        //create rotation for the character when turning
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);

        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject projectile = Instantiate(projectilePrefab, shotSpawn.transform.position, shotSpawn.transform.rotation);

            Rigidbody projectileRB = projectile.GetComponent<Rigidbody>();
            projectileRB.velocity = transform.forward * shotSpeed;
        }

        if (Input.GetKeyDown(KeyCode.Backspace) && noteOpened)//if backspace pressed and note is opened
        {
            CloseNotePanel();//close the note
        }
    }

    //create method to use part of root motion for turning
    void OnAnimatorMove()
    {
        //movement
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        //rotation
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Note"))//if we run into a note
        {
            NoteScript nScript = other.gameObject.GetComponent<NoteScript>();//reference note script

            OpenNotePanel(nScript.noteContents);
        }

        if (other.gameObject.CompareTag("CheckPoint"))
        {
            currCheckpoint = other.transform;
        }
    }

    void OpenNotePanel(string text)
    {
        noteOpened = true;//make note status open so we can close it in update

        Time.timeScale = 0f;//puase game while note it opened

        noteText.text = text;

        noteHolder.SetActive(true);//reveal note
    }

    void CloseNotePanel()
    {
        noteOpened = false;

        Time.timeScale = 1f;// unpuase game

        noteHolder.SetActive(false);
    }

    public void Reset()
    {
        transform.position = currCheckpoint.position;
    }

    //get position of last activated checkpoint

    /*/public static Vector3 GetActivatedCheckPointPosition()
    {
        Vector3 result = new Vector3(0.4f, 0, -14);

        if(CheckpointList != null)
        {
            foreach(GameObject cp in CheckPointList)
            {
                if(cp.GetComponent().activated)
                {
                    result = cp.transform.position;
                    break;
                }
            }
        }
        return result;
    }/*/
}
